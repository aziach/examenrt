package org.id.exam;

import org.id.exam.entity.Data;
import org.id.exam.entity.DataRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.Chunk;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.retry.support.RetryTemplateBuilder;
import org.springframework.transaction.PlatformTransactionManager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Configuration
public class JobConfiguartion {

    @Bean
    public Job job(JobRepository jobRepository,
                   Step step1,
                   Step step2){
        return new JobBuilder("job", jobRepository)
                .start(step1)
                .next(step2)
                .build();
    }

    @Bean
    public Step step1(JobRepository jobRepository,
                      PlatformTransactionManager transactionManager){
        return new StepBuilder("staging", jobRepository)
                .tasklet(preparingStaging(), transactionManager)
                .build();
    }

    @Bean
    @StepScope
    public Tasklet preparingStaging(){
        return new Tasklet() {
            @Override
            public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
                JobParameters jobParameters = contribution.getStepExecution().getJobParameters();
                String inputFile = jobParameters.getString("input.file");
                Path source = Paths.get(inputFile);
                Path target = Paths.get("staging", source.toFile().getName());
                Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
                return RepeatStatus.FINISHED;
            }
        };
    }

    @Bean
    public Step step2(JobRepository jobRepository,
                      PlatformTransactionManager transactionManager,
                      FlatFileItemReader<Data> flatFileItemReader,
                      ItemWriter<Data> dataItemWriter){

        int chunkSize = 0 ; // Calculez la taille du segment de 5% en fonction du nombre total de lignes dans le fichier CSV
        int totalLines = getTotalLinesInCsv("staging/dept-employee-data.csv"); // Méthode pour obtenir le nombre total de lignes du fichier CSV
        chunkSize = (int) Math.ceil(totalLines * 0.05);


        return new StepBuilder("wrinting-in-databse", jobRepository)
                .<Data,Data>chunk(chunkSize, transactionManager)
                .reader(flatFileItemReader)
                .writer(dataItemWriter)
                .faultTolerant()
                .retry(Exception.class)
                .skipLimit(7) //Le programme doit tolérer jusqu’à 7% d’erreur
                .build();


    }

    private int getTotalLinesInCsv(String filePath) {
        int totalLines = 0;

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            while (reader.readLine() != null) {
                totalLines++;
            }
        } catch (IOException e) {
            // Handle IOException, e.g., log or throw an exception
            e.printStackTrace();
        }

        return totalLines;
    }

    @Bean
    @StepScope
    public FlatFileItemReader<Data> flatFileItemReader(@Value("#{jobParameters['input.file']}") String inputFile){
        return new FlatFileItemReaderBuilder<Data>()
                .name("csv-reader")
                .resource(new FileSystemResource("staging/dept-employee-data.csv.csv"))
                .delimited()
                .names()
                .targetType(Data.class)
                .build();
    }

    @Bean
    public ItemWriter<Data> dataItemWriter(){

        return new ItemWriter<Data>() {
            @Autowired
            private DataRepository dataRepository;

            @Override
            public void write(Chunk<? extends Data> chunk) throws Exception {
                dataRepository.saveAll(chunk);
            }
        };
    }

    @Bean
    public RetryTemplate retryTemplate() {
        return new RetryTemplateBuilder()
                .maxAttempts(3)
                .build();
    }
}
