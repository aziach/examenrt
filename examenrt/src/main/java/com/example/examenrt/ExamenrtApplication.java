package com.example.examenrt;

import com.example.examenrt.config.IKafkaConstants;
import com.example.examenrt.kafka_clients.ConsumerUser;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.kafka.clients.consumer.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;

@SpringBootApplication
public class ExamenrtApplication {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(ExamenrtApplication.class, args);
        runConsumer(IKafkaConstants.TOPIC_NAME);
    }

    public static void runConsumer(String topic){
        Consumer consumer = ConsumerUser.createConsumer();
        consumer.subscribe(Arrays.asList(topic));

        try (FileWriter fw = new FileWriter("Output.csv");
             CSVPrinter csvPrinter = new CSVPrinter(fw, CSVFormat.DEFAULT.withHeader("Colonne1", "Colonne2", "Colonne3"))) {
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
                for(ConsumerRecord<String , String> record : records){
                    csvPrinter.printRecord(record.value());
                }
                csvPrinter.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            consumer.close();
        }
    }

}
