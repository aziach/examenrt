package com.example.examenrt.config;

public interface IKafkaConstants {
    public static String BROKER = "localhost:9092";
    public static String CLIENT_ID = "client1";
    public static String GROUP_ID="com.example.examenrt";
    public static String TOPIC_NAME = "final_project";
    public static String OFFSET_RESET_EAELIER = "earliest";
}
